import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { TextComponent } from './text/text.component';
import { Test1Component } from './test1/test1.component';
import { ParentModuleModule } from './parent-module/parent-module.module';
// import { ParentModulesComponent } from './parent-modules/parent-modules.component';

@NgModule({
  declarations: [
    AppComponent,
    TextComponent,
    Test1Component
    // ParentModulesComponent
  ],
  imports: [
    BrowserModule,
    ParentModuleModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { 
  
}
