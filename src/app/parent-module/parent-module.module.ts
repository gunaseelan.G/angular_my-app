import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Module1ComponentComponent } from './module1-component/module1-component.component';



@NgModule({
  declarations: [
    Module1ComponentComponent
  ],
  exports:[
    Module1ComponentComponent
  ],
  imports: [
    CommonModule
  ]
})
export class ParentModuleModule { }
